//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import Foundation

protocol SchoolDetailsViewModel {
    var pageTitle: String { get }
    var schoolNameTitle: String { get }
    var satTakersTitle: String { get }
    var mathsTitle: String { get }
    var readingTitle: String { get }
    var writingTitle: String { get }
    var satScores: SATScores? { get }
    
    func buildModel(
        onComplete: @escaping EmptySuccessBlock,
        loadingBlock: LoadingBlock
    )
}

final class SchoolDetailsViewModelImpl: SchoolDetailsViewModel {
    var pageTitle: String { "SAT Scores" }
    var schoolNameTitle: String { "School Name" }
    var satTakersTitle: String { "Total SAT takers" }
    var mathsTitle: String { "Maths Avg score:" }
    var readingTitle: String { "Reading Avg dcore:" }
    var writingTitle: String { "Writing Avg score:" }
    var satScores: SATScores? { allsatScores.first(where: { $0.dbn == schoolID }) }
    
    private var allsatScores: [SATScores] = []
    private let repo: NYCRepository
    private let schoolID: String
    init(
        schoolID: String,
        repository: NYCRepository = NYCRepositoryImpl()
    ) {
        self.repo = repository
        self.schoolID = schoolID
    }
    
    func buildModel(
        onComplete: @escaping EmptySuccessBlock,
        loadingBlock: LoadingBlock
    ) {
        repo.getSATScores(
            onComplete: { result in
                switch result {
                case .success(let allsatScores):
                    self.allsatScores = allsatScores
                    onComplete(.success(()))
                case .failure(let error):
                    onComplete(.failure(error))
                }
            },
            loadingBlock: loadingBlock
        )
    }
}
